#!/usr/bin/env python
# coding: utf-8

# In[1]:


import spacegif
from fastapi import FastAPI
from pydantic import BaseModel
from starlette.middleware.cors import CORSMiddleware


# In[2]:


app = FastAPI(title="SpaceGIF API",
    description="api for creating designs with nasa space photography",
    version="2")


# In[ ]:


origins = [
    "http:localhost",
    "http:localhost:8000",
    "http:178.128.18.109",
    "http:178.128.18.109:8000 j",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# In[4]:



class GifSpace(BaseModel):
    '''The date of the images and the message to display on image.
    date, msg. 
    {'date' : '2017-08-08', 
    'msg' : 'bookbookbook'},
    'fontdir' : '/home/pi/Downloads/Arial.ttf',
    'blogdir' : '/home/pi/artctrl'
    '''
    date: str = '2017-08-08'
    msg: str = 'bookbookbook'
    fontdir: str = '/home/pi/Downloads/Arial.ttf'
    blogdir: str = '/home/pi/artctrl'
        #fontdir, fontsize, blogdir


# In[9]:


@app.post('/reqearth')
def reqspc(blogdir: str,date: str):
    '''Get the earth images for that day.'''
    spacegif.reqspace(blogdir, date)
    return({"status": "ok"})


# In[ ]:


@app.post('/reqmars')
def reqmar(blogdir: str,date: str):
    '''Get the mars images for that day.'''
    spacegif.reqmars(blogdir, date)
    return({"status": "ok"})


# In[ ]:


@app.post('/merge')
def merge(blogdir: str,date: str,):
    '''merge the earth and mars images together'''
    spacegif.mergeearthmars(blogdir, date)
    return({"status": "ok"})


# In[ ]:


@app.post('/downloadnmerge')
def dwnmerg(blogdir: str,date: str):
    spacegif.reqspace(blogdir, date)
    spacegif.reqmars(blogdir, date)
    spacegif.mergeearthmars(blogdir, date)
    return({"status": "ok"})


# In[10]:


@app.post('/gspace')
#gifspace(fontdir, thedate, msg, sdir)
def gspace(blogdir: str, fontdir: str, date: str, msg: str):
    '''The date of the images and the message to display on image.
    date, msg.'''
    return(spacegif.gifspace(blogdir, fontdir, date, msg))
    

    #blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d') + '/texts/' 


# {'fontdir' : '/home/pi/Downloads/Arial.ttf',
# 'date' : '2017-08-08', 
# 'msg' : 'bookbookbook',
# 'blogdir' : '/home/pi/artctrl'}

# In[ ]:




