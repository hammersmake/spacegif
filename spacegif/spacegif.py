#!/usr/bin/env python
# coding: utf-8

# In[4]:


import json
import requests
import getpass
import shutil
import os
import PIL
import imageio
import textwrap
import random 
import requests 
import arrow
import shutil
import datetime
#import nltk
import getpass
import json
#import PIL
import random
from PIL import ImageDraw, ImageFont, Image, ImageFilter
#from IPython.display import Image as disimg
from urllib.parse import urlparse
import pandas as pd
import arrow
from shutil import copyfile
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw


# In[5]:


from pathlib import Path
home = str(Path.home())


# In[ ]:





# In[2]:


with open('cred.txt', 'r') as apikey:
    apikey = apikey.read().strip('\n')


# In[ ]:





# In[21]:


def Enquiry(lis1): 
    if not lis1: 
        return 1
    else: 
        return 0
          
# Driver Code 


# In[4]:


def reqmars(blogdir, thedate):
    #print('https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?earth_date=' + thedate + '&api_key=' + apikey )
    marsreq = requests.get('https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?earth_date=' + thedate + '&api_key=' + apikey )
    marjs = marsreq.json()
    if not marjs['photos']:
        raise Exception("No rover images for {}".format(thedate))
    else:
        camset = list()
        for alres in marjs['photos']:
            camset.append(alres['camera']['name'])
        randcam = random.choice(list(set(camset)))
        #print(randcam)
        dateinput = arrow.get(thedate)
        try:
            os.makedirs(blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d' + '/rovers/{}/'.format(randcam)))
        except FileExistsError:
            pass
        for marpho in range(0, len(marjs['photos'])):
            if marjs['photos'][marpho]['camera']['name'] == randcam:


                #froncam.append(marjs['photos'][marpho]['img_src'])
                response = requests.get(marjs['photos'][marpho]['img_src'], stream=True)
                with open(blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d') + '/rovers/' + randcam + '/' + str(marjs['photos'][marpho]['id']) + '.jpg', 'wb') as out_file:
                    shutil.copyfileobj(response.raw, out_file)
                    del response
            else:
                pass

        #print()


# In[6]:


def ranmars(blogdir, thedate, camera):
    #returns a random image of mars for that day and camera
    dateinput = arrow.get(thedate)
    foldate = dateinput.strftime('%Y/%m/%d')
    return(random.choice(os.listdir('{}/galleries/{}/mars-{}-template'.format(blogdir, foldate, camera))))


# In[7]:


def makblenfold(blogdir, thedate):
    dateinput = arrow.get(thedate)
    foldate = dateinput.strftime('%Y/%m/%d')
    os.makedirs('{}/galleries/{}/blend/'.format(blogdir, foldate))


# In[8]:


def marsgify(blogdir, thedate, camera, perid):
    #dateinput = arrow.get(thedate)
    
    i = pd.date_range(thedate, periods=perid, freq='1D')
    for t in i:
        #print(t)
        dateinput = arrow.get(t)
        #print(dateinput.date())
        for simg in (os.listdir(blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d') + '/mars-' + camera + '-template/')):
            print(simg)
            copyfile(blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d') + '/mars-' + camera + '-template/' + simg, '/home/pi/margif/' + simg)
            
    earthspin = os.listdir('{}/margif/'.format(home))
    images = list()
    for filen in earthspin:
        images.append(imageio.imread('{}/margif/{}'.format(home, filen)))
    imageio.mimsave('{}/tests/mars.gif'.format(home), images, fps=3)

    
#takes date, amount of days, and then turns those mars photos into gif. 


# In[9]:


#marlapse('/home/pi/artctrl', '2019/01/01', 12)


# In[10]:


def checkmardate(thedate):
    '''
    give it a date and it checks that mars rover / earth photos for that day exist. Also returns what type of 
    rover images are there.
    '''
    marsreq = requests.get('https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?earth_date=' + thedate + '&api_key=' + apikey )
    marjs = marsreq.json()
    if not marjs['photos']:
        print ("No rover images for {}".format(thedate))
    else:
        camset = list()
        for alres in results['photos']:
            camset.append(alres['camera']['name'])
        print (set(camset))

    


# In[11]:


#marsgify('2019/01/01', 'RHAZ', 12)


# In[1]:


def reqspace(blogdir, thedate):
    
    rqnat = requests.get('https://epic.gsfc.nasa.gov/api/natural/date/{}?api_key={}'.format(thedate, apikey))
    #print('https://epic.gsfc.nasa.gov/api/natural/date/{}?api_key={}'.format(thedate, apikey))
    #print(rqnat.json())
    #return(rqnat.json())
    rjsn = rqnat.json()
    #return(rjsn)
    
    if Enquiry(rjsn): 
        raise Exception("No imgs")
    else: 
        print ("The list is not empty") 
        lenet = len(rjsn)
        somnewd = arrow.get(rjsn[0]['date'])
        images = []
        dateinput = arrow.get(thedate)
        try:
            os.makedirs(blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d' + '/template'))
        except FileExistsError:
            pass


        for somn in range(0, lenet):
            urlearth = 'https://epic.gsfc.nasa.gov/archive/natural/' + somnewd.strftime('%Y/%m/%d') + '/png/' + rjsn[somn]['image'] + '.png'
            #print(urlearth )
            response = requests.get(urlearth, stream=True)
            with open('{}/{}.png'.format(blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d' + '/template/'), rjsn[somn]['image']), 'wb') as out_file:
                shutil.copyfileobj(response.raw, out_file)
                del response
        return(rqnat.json())


# In[13]:


#enter date and amount of days to get. creates gif from the photos taken over those days.


# In[14]:


def marlapse(blogd, date, perid):
    i = pd.date_range(date, periods=perid, freq='1D')
    for t in i:
        #print(t)
        artim = arrow.get(t)
        print(artim.date())
        reqmars(blogd, str(artim.date()), 'RHAZ')


# In[15]:


#marlapse('2018/01/01', 7)


# In[16]:


def mergeearthmars(blogdir, thedate):
    #give it date to merge. 
    dateinput = arrow.get(thedate)
    foldate = dateinput.strftime('%Y/%m/%d')
    #merge the earth and mars images together. 
    spaceimg = os.listdir('{}/galleries/{}/template/'.format(blogdir, foldate))
    ranc = random.choice(os.listdir('{}/galleries/{}/rovers/'.format(blogdir, foldate)))
    try:
        os.makedirs(blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d') + '/blend/')
    except FileExistsError:
        pass

    rancam = '{}/galleries/{}/rovers/{}/'.format(blogdir, foldate, ranc)
    #print(rancam)
    fileran = random.choice(os.listdir(rancam))
    ranmar = '{}/{}'.format(rancam, fileran)
    marimg = PIL.Image.open('{}'.format(ranmar))
    marimg = marimg.resize([512,512])
    marimg = marimg.convert('RGB')
    for spci in spaceimg:
        earimg = PIL.Image.open('{}/galleries/{}/template/{}'.format(blogdir, foldate, spci))
        earimg= earimg.resize([512,512])
        earimg = earimg.convert('RGB')
        alphaBlended1 = Image.blend(earimg, marimg, alpha=.5)
        alphaBlended1.save('{}/galleries/{}/blend/{}'.format(blogdir, foldate, spci))
    #with open('spacegif/' + dateinput.strftime('%Y/%m/%d') + '/mars-' + camera + '-template/' + str(marjs['photos'][marpho]['id']) + '.jpg', 'wb') as out_file:
    #alphaBlended1 = Image.blend(earimg, marimg, alpha=.3)
    return({"status": "ok"})


# In[17]:


#makblenfold('/home/pi/sg', '2018-11-10')


# In[18]:


#mergeearthmars('/home/pi/sg', '2020-11-01')


# In[19]:


def timelinepace(thedate, msg, sdir):
    dateinput = arrow.get(thedate)
    msgslug = msg.replace(' ', '-')
    try:
        os.makedirs(sdir + dateinput.strftime('%Y/%m/%d') + '/texts/' + msgslug)
    except FileExistsError:
        pass
    
    
    spaceimg = os.listdir(sdir + dateinput.strftime('%Y/%m/%d') + '/template/')
    for spci in spaceimg:
        earim = PIL.Image.open(sdir + dateinput.strftime('%Y/%m/%d') + '/template/' + spci)
        earsm = earim.resize([512,512])
        earimg = earsm.convert("RGBA")
        #alphaBlended1 = Image.blend(earimg, marimg, alpha=.3)
        font = ImageFont.truetype("{}/Downloads/Arial.ttf".format(home),42)

            #draw = ImageDraw.Draw(earimg)
           #draw.text((0, 0),msg[0:12] + ' \n' + msg[12:24] + ' \n' + msg[24:36],(255, 255, 255),font=font)

        draw = ImageDraw.Draw(earimg)
        image_width, image_height = earimg.size
        #if randomove == 'Y':
        #    y_text = 512 / random.randint(1,3)
        #else:
        y_text = 256
        print(y_text)
        lines = textwrap.wrap(msg, width=20)
        for line in lines:
            line_width, line_height = font.getsize(line)
            draw.text(((image_width - line_width) / 2, y_text), 
                      line, font=font, fill=(255,255,255))
            y_text += line_height
    #alphaBlended1.save('gif/{}-{}.png'.format(thedate, somn))
    #draw.text((0, 0),"Sample Text",(255,255,255),font=font)
    #img.save('sample-out.jpg')
        #print(spci)

        earimg.save(sdir + '/galleries/' + dateinput.strftime('%Y/%m/%d') + '/texts/' + msgslug + '/' +(spci))

    earthspin = os.listdir(sdir + '/galleries/' + dateinput.strftime('%Y/%m/%d') + '/texts/' + msgslug + '/')
    images = list()
    for filen in earthspin:
        images.append(imageio.imread('spacegif/' + dateinput.strftime('%Y/%m/%d') + '/texts/' + msgslug + '/' + filen))
    imageio.mimsave('spacegif/' + dateinput.strftime('%Y/%m/%d') + '/texts/' + msgslug + '.gif', images, fps=3)


# In[20]:


def gifspace(blogdir, fontdir, thedate, msg):
    dateinput = arrow.get(thedate)
    msgslug = msg.replace(' ', '-')
    try:
        os.makedirs(blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d') + '/texts/' + msgslug)
    except FileExistsError:
        pass
    
    try:
        os.makedirs(blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d') + '/blend/')
    except FileExistsError:
        pass
    
    
    spaceimg = os.listdir(blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d') + '/blend/')
    for spci in spaceimg:
        earim = PIL.Image.open(blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d') + '/blend/' + spci)
        earsm = earim.resize([512,512])
        earimg = earsm.convert("RGBA")
        #alphaBlended1 = Image.blend(earimg, marimg, alpha=.3)
        font = ImageFont.truetype(fontdir,42)

            #draw = ImageDraw.Draw(earimg)
           #draw.text((0, 0),msg[0:12] + ' \n' + msg[12:24] + ' \n' + msg[24:36],(255, 255, 255),font=font)

        draw = ImageDraw.Draw(earimg)
        image_width, image_height = earimg.size
        #if randomove == 'Y':
        #    y_text = 512 / random.randint(1,3)
        #else:
        y_text = 256
        print(y_text)
        lines = textwrap.wrap(msg, width=20)
        for line in lines:
            line_width, line_height = font.getsize(line)
            draw.text(((image_width - line_width) / 2, y_text), 
                      line, font=font, fill=(255,255,255))
            y_text += line_height
    #alphaBlended1.save('gif/{}-{}.png'.format(thedate, somn))
    #draw.text((0, 0),"Sample Text",(255,255,255),font=font)
    #img.save('sample-out.jpg')
        #print(spci)

        earimg.save(blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d') + '/texts/' + msgslug + '/' +(spci))

    earthspin = os.listdir(blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d') + '/texts/' + msgslug + '/')
    images = list()
    for filen in earthspin:
        images.append(imageio.imread(blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d') + '/texts/' + msgslug + '/' + filen))
    imageio.mimsave(blogdir + '/galleries/' + dateinput.strftime('%Y/%m/%d/') + msgslug + '.gif', images, fps=6)
    return({"url": "http://localhost:5431/" + dateinput.strftime('%Y/%m/%d/') + msgslug + '.gif'})


# In[21]:


#gifspace('/home/pi/sg', '/home/pi/Downloads/1942.ttf', '2020-11-01', 'hellodaddy')


# In[22]:


#gifspace('/home/pi/ARIALBD.TTF', '2018-11-10', 'this is a new message', '/home/pi/sg')


# In[23]:


def listoimg(thedate, msglist):
    mlist = msglist.split (",")
    print(mlist)
    for llistofth in mlist:
        gifspace(thedate, llistofth)
        
        
        print(llistofth)


# In[24]:


#listoimg('2019-03-15', 'not moving,the text')


# In[25]:


#gifspace('2019-01-30', 'William MckeeIT', '/home/pi/git/spacegif/spacegif/spacegif/')


# In[26]:


#These functions below work.
#reqmars('/home/williammckee/sgblog', '2019-01-01', 'RHAZ')


# In[27]:


#reqspace('/home/williammckee/sgblog', '2019-01-01')


# In[28]:


#ranmars('/home/williammckee/sgblog', '2018-08-10', 'RHAZ')


# In[33]:


#makblenfold('/home/williammckee/sgblog', '2019-01-01')


# In[34]:


#mergeearthmars('/home/williammckee/sgblog', '2019-01-01', 'RHAZ')


# In[35]:


#gifspace('2019-01-01', 'Sydney Python', '/home/williammckee/sgblog/galleries/')

